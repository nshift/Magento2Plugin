<?php
/**
 * A Magento 2 module named Nshift/Integration
 * Copyright (C) 2017  Nshift 2018
 *
 * This file is part of Nshift/Integration.
 *
 * Nshift/Integration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Nshift\Integration\Block;

class Config extends \Magento\Framework\View\Element\Template {

    protected $_varFactory;

    public function __construct(
        \Magento\Variable\Model\VariableFactory $varFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        \Nshift\Integration\Helper\Data $helperData
    )
    {
        $this->_varFactory = $varFactory;
        $this->_helperData = $helperData;
        parent::__construct($context);
    }

    public function getToken() {
        // Attribute qa or prod token
        $token = $this->_helperData->getToken();

        if(!empty($this->_helperData->getDebug())) {
            $token = $this->_helperData->getQAToken();
        }

        if(!empty($token)) {
            return $token;
        } else {
            return 'no-token';
        }
    }

    public function getIntegrationUrl() {
        // Attribute qa or prod token
        $webservice_endpoint = 'https://integration.consignor.com';

        if(!empty($this->_helperData->getDebug())) {
            $webservice_endpoint = 'http://int.qa.consignor.com';
        }

        return $webservice_endpoint;
    }

    public function getVersion() {
        $version = $this->_helperData->getVersion();

        if(!empty($version)) {
            return $version;
        } else {
            return 'N/A';
        }
    }
}