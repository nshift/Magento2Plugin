<?php
/**
 * PickupLayoutProcessor
 *
 * @copyright Copyright © 2020 De Facto. All rights reserved.
 * @author    developers@ede-facto.com
 */

namespace Nshift\Integration\Block\Checkout;

use Nshift\Integration\Helper\Data;
use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;

class PickupLayoutProcessor implements LayoutProcessorInterface
{
    /**
     * @var Data
     */
    protected $helper;

    public function __construct(
        Data $helper
    ) {
        $this->helper = $helper;
    }

    public function process($jsLayout)
    {
        if ($this->helper->isShipAdviseEnabled()) {
            if (isset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
                ['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']
                ['children']['pickup-address-form-fields'])) {

                $pickupForm = $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
                    ['children']['shippingAddress']['children']['before-form']['children']
                    ['pickup-point-form-container']['children']['pickup-address-form-fields'];

                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
                    ['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']
                    ['children']['map-form-fields'] = $pickupForm;

                $mapFormFields = &$jsLayout['components']['checkout']['children']['steps']['children']
                    ['shipping-step']['children']['shippingAddress']['children']['before-form']['children']
                    ['pickup-point-form-container']['children']['map-form-fields'];

                $mapFormFields['displayArea'] = 'map-form-fields';

                foreach ($mapFormFields['children'] as $mapFormField) {
                    $mapFormField['validation'] = [];
                }
            }
        }

        return $jsLayout;
    }
}
