define([
    'jquery',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'Magento_Checkout/js/model/shipping-service',
], function (
    $,
    checkoutData,
    addressConverter,
    selectShippingAddress,
    addressList,
    rateRegistry,
    shippingService
) {
    'use strict';

    return {
        getAddressFromAddressList: function () {
            if (addressList().length) {
                return addressList()[0];
            }
            return false;
        },
        recollectRatesFromPickupAddress: function () {
            var pickupAddress = checkoutData.getPickupAddressFromData(),
                isDropPointSelected = checkoutData.getSelectedDropPointId();
            shippingService.isLoading(true);
            if (!pickupAddress || !isDropPointSelected) {
                shippingService.isLoading(false);
                return;
            }

            var addressData = $.extend(
                pickupAddress,
                {
                    save_in_address_book: 0,
                }
            ), address = addressConverter.formAddressDataToQuoteAddress(addressData);
            selectShippingAddress(address);
        },
        recollectRatesFromPostalAddress: function () {
            var postalAddress = checkoutData.getShippingAddressFromData(),
                existingAddressFromList = this.getAddressFromAddressList(),
                address = addressConverter.formAddressDataToQuoteAddress({});
            shippingService.isLoading(true);
            if (existingAddressFromList) {
                address = existingAddressFromList;
            } else if (!$.isEmptyObject(postalAddress)) {
                address = addressConverter.formAddressDataToQuoteAddress(postalAddress);
            }
            selectShippingAddress(address);
        }
    }
});
