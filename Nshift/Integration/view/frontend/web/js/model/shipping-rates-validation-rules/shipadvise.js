
define([], function () {
    'use strict';

    return {
        /**
         * @return {Object}
         */
        getRules: function () {
            return {
                'city': {
                    'required': true,
                },
                'street': {
                    'required': true
                },
                'country_id': {
                    'required': true
                },
                'postcode': {
                    'required': true
                },
                'region_id': {
                    'required': true
                },
                'region_id_input': {
                    'required': true
                }
            };
        }
    };
});
