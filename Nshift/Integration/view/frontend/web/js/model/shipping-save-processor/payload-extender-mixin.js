define([
    'mage/utils/wrapper',
    'jquery',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/checkout-data',
    'Nshift_Integration/js/model/deliver_to'
], function (wrapper, $, quote, checkoutData, deliverTo) {
    'use strict';

    return function (payloadExtender) {
        return wrapper.wrap(payloadExtender, function (parentPayloadExtender, payload) {
            var payLoadData = parentPayloadExtender(payload);
            if (typeof window.isShipadviseEnabled !== 'undefined' && window.isShipadviseEnabled === false) {
                return payLoadData;
            }

            if (typeof payLoadData === 'object' &&
                payLoadData.hasOwnProperty('addressInformation') &&
                payLoadData.addressInformation.hasOwnProperty('shipping_address')
            ) {
                var attributes = quote.shippingMethod()['extension_attributes'];
                attributes = attributes.hasOwnProperty('additional') ? JSON.parse(attributes.additional) : null;

				if (attributes) {
					payLoadData.addressInformation.shipping_address['customAttributes'] = $.extend(
						{},
						payLoadData.addressInformation.shipping_address['customAttributes'],
						{
                            delivery_date: attributes.delivery_date,
                            dispatch_date: attributes.dispatch_date,
							carrier: attributes.carrier,
							carrier_display_name: attributes.carrier_display_name,
						}
					);
				}

                if (deliverTo.checkDeliverTo() === 'pickup') {
                    payLoadData.addressInformation.shipping_address = $.extend(
                        payLoadData.addressInformation.shipping_address,
                        _.pick(checkoutData.getPickupAddressFromData(), Object.keys(payLoadData.addressInformation.shipping_address))
                    );
                }
            }

            return payLoadData;
        });
    };
});
