define([
    'mage/utils/wrapper',
], function (wrapper) {
    return function (BaseAddress) {

        return wrapper.wrap(BaseAddress, function (parentBaseAddress, address) {
            var $return = parentBaseAddress();

            if (window.isShipadviseEnabled !== true) {
                return $return;
            }

            if (typeof window.checkoutConfig === 'object' && typeof requirejs === 'function') {
                var deliverTo  = requirejs('Nshift_Integration/js/model/deliver_to');
                $return.extension_attributes = {};

                $return.canUseForBilling = function () {
                    return deliverTo.getValue() !== 'pickup';
                };

                $return.showInAddressList = function () {
                    return deliverTo.getValue() !== 'pickup';
                };

                $return.getExtensionAttributes = function () {
                    return address.extension_attributes;
                };
            }
            return $return;
        });
    };
});
