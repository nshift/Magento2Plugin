/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'ko',
    'jquery',
    'Magento_Checkout/js/checkout-data'
], function (ko, $, checkoutData) {
    return {
        firstname: ko.observable(),
        lastname: ko.observable(),
        telephone: ko.observable(),
        email: ko.observable(),
        city: ko.observable(),
        country_id: ko.observable(),
        region_id: ko.observable(),
        postcode: ko.observable(),
        /**
         *
         * @param data {object}
         * @returns {boolean|object}
         */
        setData: function (data) {
            if (typeof data !== 'object') {
                console.error('setData requires an object to be passed in.');
                return false;
            }
            data = $.extend({}, this.getData(), data);
            for (var prop of Object.keys(data)) {
                if (typeof this[prop] !== 'undefined') {
                    this[prop](data[prop]);
                }
            }
            return this;
        },
        /**
         *
         * @returns {object}
         */
        getData: function () {
            return {
                firstname: this.firstname(),
                lastname: this.lastname(),
                telephone: this.telephone(),
                email: this.email(),
                city: this.city(),
                country_id: this.country_id(),
                region_id: this.region_id(),
                postcode: this.postcode()
            }
        },
        isValid: false,
        setIsValid: function (value) {
            this.isValid = value;
        },
        getIsValid: function () {
            return this.isValid;
        },
        getPersistedValues: function() {
            if(checkoutData.pickup) {
                var data = checkoutData.pickup;
            } else {
                var data = {
                    firstname: 'Persisted',
                    lastname: '',
                    telephone: '',
                    email: '',
                    city: '',
                    country_id: '',
                    region_id: '',
                    postcode: ''
                };
            }

            this.setData(data);

            //var viewModel = ko.mapping.fromJS(checkoutData.pickup);
            //ko.applyBindings(viewModel);
        }
    }
});
