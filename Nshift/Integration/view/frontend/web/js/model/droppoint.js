/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'ko',
    'underscore',
    'jquery',
    'mage/url',
    'mage/translate',
    'Nshift_Integration/js/model/pickup',
    'Nshift_Integration/js/model/map-popup-state',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/action/select-shipping-address',
    'uiRegistry',
    'Magento_Checkout/js/model/address-converter',
    'mage/template',
    'text!Nshift_Integration/template/ui/map/info-window.html',
], function (
    ko,
    _,
    $,
    urlBuilder,
    $t,
    pickup,
    mapPopupState,
    checkoutData,
    selectShippingAddress,
    registry,
    addressConverter,
    mageTemplate,
    infoWindowTpl
) {
    var cache = [], mapOptions = { };
    if (window.checkoutConfig.hasOwnProperty('googleMapOptions')) {
        mapOptions = $.extend({}, mapOptions, window.checkoutConfig.googleMapOptions);
    }
    if (typeof window.Consignor === 'undefined') {
        window.Consignor = {};
    }
    window.Consignor.Map = {
        id: ko.observable(null),
        isDropPointsLoading: ko.observable(false),
        isAdditionalAttributesSaved: ko.observable(false),
        errorMessage: ko.observable(false),
        hasDropPointBeenSelected: ko.observable(false),
        mapOptions: {
            infoWindowTemplate: infoWindowTpl
        },
        selectDropPoint: function (jsonData) {
            var data = JSON.parse(jsonData);
            if (typeof data === 'object') {
                this.savePickupAddress(data);
                this.hasDropPointBeenSelected(true);
                mapPopupState.isVisible(false);
            }
        },
        getDropPoint: function () {
            var droppoint_data = checkoutData.getDropPointData();

            if (droppoint_data && droppoint_data.id) {
                return droppoint_data;
            } else {
                return null;
            }
        },
        getDropPoints: function (formData) {
            var droppoint_model = this;
            this.isDropPointsLoading(true);
            var cacheKey = 'GetAllDropPoints' + $.param(formData),
                foundCache = _.findWhere(cache, { cacheKey: cacheKey });
            if (foundCache && !droppoint_model.errorMessage()) {
                this.isDropPointsLoading(false);
                mapPopupState.isVisible(true);
            } else {
                $.ajax({
                    type: 'POST',
                    url: urlBuilder.build('nshiftintegration/eshopper/GetAllDropPoints'),
                    data: $.param(formData),
                    dataType: 'json',
                    success: function (resp, textStatus, jqXHR) {
                        var html = '';

                        if (resp.error) {
                            droppoint_model.errorMessage($t(resp.error));
                            return;
                        }
                        // If we have drop points in result and DeliverTo is Pickup
                        if (!Array.isArray(resp.drop_points) || (Array.isArray(resp.drop_points) && resp.drop_points.length === 0)) {
                            droppoint_model.errorMessage($t('No collection points could be found.'));
                        }

                        cache.push({
                            cacheKey: cacheKey,
                            dropPoints: resp.drop_points
                        });

                        if (Array.isArray(resp.drop_points) && resp.drop_points.length) {
                            mapPopupState.isVisible(true);

                            delete resp.drop_points.center;

                            var location = [];

                            for (key in resp.drop_points) {

                                var mark = resp.drop_points[key];

                                var coordinates_lat = mark.coordinates['lat'];
                                var coordinates_lon = mark.coordinates['lng'];
                                var name = mark.name;
                                var id = mark.id;
                                //check for default
                                var default_hours = mark.operational_hours.default;
                                var has_default = false;
                                var weekdays = {'mon':'Monday','tue':'Tuesday','wed':'Wednesday','thu':'Thuesday','fri':'Friday','sat':'Saturday','sun':'Sunday'};

                                for (var hours in default_hours) {
                                    if (default_hours.hasOwnProperty(hours)) {
                                        var openning_default = default_hours[0];
                                        has_default = true;
                                    }
                                }

                                //check for weekdays
                                var week_hours = mark.operational_hours.overrides.weekday;
                                var has_week = false;

                                for (var hours in week_hours) {
                                    if (week_hours.hasOwnProperty(hours)) {
                                        has_week = true;
                                    }
                                }

                                var schedule_html = '<div class="hours-block iw-content-padding iw-content-border">';

                                if (has_week) {
                                    for (week in weekdays) {
                                        if (week_hours.hasOwnProperty(week)) {
                                            var hours_array   = week_hours[week][0];

                                            schedule_html += '<div class="hours-main"><div class="hours-day">'+weekdays[week]+':</div><div class="hours-schedule">'+hours_array['open']+'-'+hours_array['close']+'</div></div>';
                                        } else {
                                            if (has_default) {
                                                schedule_html += '<div class="hours-main"><div class="hours-day">'+weekdays[week]+':</div><div class="hours-schedule">'+openning_default['open']+'-'+openning_default['close']+'</div></div>';
                                            }
                                        }

                                    }
                                }

                                schedule_html += '</div>';

                                var address = [];
                                if (mark.address.hasOwnProperty('street1') && mark.address.street1 !== '') {
                                    address.push(mark.address.street1);
                                }
                                if (mark.address.hasOwnProperty('street2') && mark.address.street2 !== '') {
                                    address.push(mark.address.street2);
                                }
                                if (mark.address.hasOwnProperty('city') && mark.address.city !== '') {
                                    address.push(mark.address.city);
                                }
                                if (mark.address.hasOwnProperty('region') && mark.address.region !== '') {
                                    address.push(mark.address.region);
                                }
                                if (mark.address.hasOwnProperty('postcode') && mark.address.postcode !== '') {
                                    address.push(mark.address.postcode);
                                }
                                if (mark.address.hasOwnProperty('country_code') && mark.address.country_code !== '') {
                                    address.push(mark.address.country_code);
                                }
                                var content = mageTemplate(droppoint_model.mapOptions.infoWindowTemplate, {
                                    data: {
                                        marker: mark,
                                        scheduleHtml: schedule_html,
                                        coordinates: {
                                            lat: coordinates_lat,
                                            lng: coordinates_lon
                                        },
                                        addressHtml: address.join('<br />'),
                                        title: id,
                                        droppoint: mark,
                                        iconImage: droppoint_model.getCarrierImage(mark.product.carrier_name.toLowerCase()),
                                        selectButtonText: $t('Select'),
                                        openingText: $t('Opening Times')
                                    }
                                });

                                var marker_options = {
                                    coordinates: {coordinates_lat, coordinates_lon},
                                    iconImage: droppoint_model.getCarrierImage(mark.product.carrier_name.toLowerCase()),
                                    content: content,
                                    title: id,
                                    droppoint: mark
                                };

                                location.push(marker_options);
                            }

                            window.map = new google.maps.Map(document.getElementById('map'), mapOptions);

                            var infowindow = new google.maps.InfoWindow();

                            var bounds = new google.maps.LatLngBounds();

                            for (i = 0; i < location.length; i++) {
                                marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(location[i].coordinates.coordinates_lat, location[i].coordinates.coordinates_lon),
                                    map: map, //Map that we need to add
                                    draggable: false, // If set to true you can drag the marker
                                    icon: location[i].iconImage,
                                    title: location[i].title,
                                    droppoint: location[i].droppoint,
                                    content: location[i].content,
                                });

                                bounds.extend(marker.position);

                                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                                    return function () {
                                        // - Show content in a tooltip on the map
                                        $('#info').trigger('markerClicked', marker);
                                        $('#info').html(
                                            "<div id='shop-window'>" +
                                            '<script type="application/json" id="'+marker.droppoint.id+'">' +
                                            JSON.stringify(marker.droppoint) +
                                            '</script>' +
                                            marker.content +
                                            '</div>'
                                        );
                                        $('#info').find('[data-role="toggleTimesBtn"]').attr(
                                            'onclick',
                                            'window.Consignor.Map.toggleTimes()'
                                        );
                                        $('#info').find('button[data-drop-point-id="'+marker.droppoint.id+'"]').attr(
                                            'onclick',
                                            'window.Consignor.Map.selectDropPoint(document.getElementById(\''+marker.droppoint.id+'\').innerHTML)'
                                        );
                                    }
                                })(marker, i));
                            }

                            map.fitBounds(bounds);
                            cache[cacheKey] = resp.drop_points;
                        }
                    },
                    complete: function (data) {
                        droppoint_model.isDropPointsLoading(false);
                    }
                });
            }
        },
        toggleTimes: function () {
            $('#info').find('.shop-middle .store-times').toggle();
        },
        getCarrierImage: function (carrier_name) {
            var carriers = {
                'generic': $('#default_marker').val(),
                'fpcnc': $('#fpcnc_marker').val(),
                'dpd': $('#dpd_marker').val(),
                'ups': $('#ups_marker').val(),
                'hermes': $('#hermes_marker').val(),
                'dhl': $('#dhl_marker').val(),
                'dhlexp': $('#dhl_exp_marker').val(),
                'bring': $('#bring_marker').val(),
                'dao': $('#dao_marker').val(),
                'gls': $('#gls_marker').val(),
                'postnord': $('#postnord_marker').val(),
                'best': $('#best_marker').val(),
                'benum': $('#benum_marker').val()
            };

            if (carriers[carrier_name]) {
                return carriers[carrier_name];
            } else {
                return carriers['generic'];
            }
        },
        savePickupAddress: function (data) {
            // Save to local storage
            checkoutData.setDropPointData(data);
            var product = data.product,
                dispatch_date = typeof product === 'object' ? product.cutoff_time : null,
                delivery_date = typeof product === 'object' ? product.delivery_date : null,
                carrier = typeof product === 'object' ? product.carrier_name : null;
                carrier_display_name = typeof product === 'object' ? product.carrier_display_name : null;

            if (!dispatch_date || !delivery_date || !carrier || !carrier_display_name) {
                console.error('Missing shipping service data. Please contact website administrator.');
            }

            // Save address fields
            var addressFlat = {
                company: data.name,
                street: [data.address.street1],
                postcode: data.address.postcode,
                city: data.address.city,
                custom_attributes: {
                    droppoint_id: data.id,
                    dispatch_date: dispatch_date,
                    delivery_date: delivery_date,
                    carrier: carrier,
                    carrier_display_name: carrier_display_name
                }
            };

            checkoutData.setPickupAddressFromData(
                $.extend(true, {}, checkoutData.getPickupAddressFromData(), addressFlat)
            );
            registry.async('checkoutProvider')(function (checkoutProvider) {
                var pickupAddressData = checkoutData.getPickupAddressFromData();
                checkoutProvider.set(
                    'pickupPointForm',
                    $.extend(true, {}, checkoutProvider.get('pickupPointForm'), pickupAddressData)
                );
            });

            addressFlat = checkoutData.getPickupAddressFromData();
            var address = addressConverter.formAddressDataToQuoteAddress(addressFlat);
            // Refresh shipping methods
            selectShippingAddress(address);

            this.id(data.id);
            checkoutData.setSelectedDropPointId(data.id);
        },
    };

    return window.Consignor.Map;
});
