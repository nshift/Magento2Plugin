define([
    'mage/utils/wrapper',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/action/select-shipping-address',
    'Nshift_Integration/js/model/pickup',
    'Nshift_Integration/js/model/droppoint',
], function (
    wrapper,
    checkoutData,
    addressConverter,
    selectShippingAddress,
    pickup,
    droppoint
) {
    'use strict';

    return function (checkoutDataResolver) {
        if (typeof window.isShipadviseEnabled !== 'undefined' && window.isShipadviseEnabled === false) {
            return checkoutDataResolver;
        }
        checkoutDataResolver.resolveShippingAddress = wrapper.wrapSuper(checkoutDataResolver.resolveShippingAddress, function () {
            var isPickup = checkoutData.getDeliverTo() === 'pickup',
                pickupAddressData = checkoutData.getPickupAddressFromData(),
                selectedDropPointId = checkoutData.getSelectedDropPointId();
            if (pickupAddressData) {
                pickup.setData(pickupAddressData);

                if (selectedDropPointId) {
                    droppoint.hasDropPointBeenSelected(true);
                }

                if (isPickup) {
                    var pickupAddress = addressConverter.formAddressDataToQuoteAddress(pickupAddressData);
                    selectShippingAddress(pickupAddress);
                    return;
                }
            }

            return this._super();
        });

        return checkoutDataResolver;
    };
});
