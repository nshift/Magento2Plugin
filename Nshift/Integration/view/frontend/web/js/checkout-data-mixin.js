define([
    'jquery',
    'Magento_Customer/js/customer-data',
    'jquery/jquery-storageapi'
], function ($, storage) {
    return function(checkoutData) {
        var cacheKey = 'checkout-data';

        var getData = function() {
            var data = storage.get(cacheKey)();

            if($.isEmptyObject(data)) {
                data = $.initNamespaceStorage('mage-cache-storage').localStorage.get(cacheKey);
            }

            return data;
        };

        var cartData = storage.get('cart'),
            storeId = null;

        if (typeof cartData === 'function' && typeof cartData() === 'object') {
            storeId = cartData().storeId;
        }

        var saveData = function(data) {
            storage.set(cacheKey, data);
        }

        // Deliver to
        checkoutData.deliverTo = 'address';

        checkoutData.setDeliverTo = function(deliverTo) {
            var obj = getData();
            obj.deliverTo = deliverTo;
            saveData(obj);
        };

        checkoutData.getDeliverTo = function() {
            var data = getData();

            if(typeof data === 'undefined' || (typeof data === 'object' && !data.hasOwnProperty('deliverTo'))) {
                return checkoutData.deliverTo;
            }

            return data.deliverTo;
        }

        // Drop point data
        checkoutData.droppoint_data = null;

        checkoutData.setDropPointData = function(droppoint_data) {
            var obj = getData();
            if (droppoint_data === null || droppoint_data === undefined) {
                obj.droppoint_data = droppoint_data;
                saveData(obj);
                return checkoutData;
            }
            obj.droppoint_data = droppoint_data;
            saveData(obj);
        };

        checkoutData.getDropPointData = function() {
            var data = getData();

            if(typeof data === 'undefined' || (typeof data === 'object' && !data.hasOwnProperty('droppoint_data'))) {
                return checkoutData.droppoint_data;
            }

            return data.droppoint_data;
        };

        checkoutData.setPickupAddressFromData = function (data) {
            var obj = getData();
            obj.pickupAddress = data;
            if (obj.pickupAddress && !obj.pickupAddress.storeId) {
                obj.pickupAddress.storeId = storeId;
            }
            saveData(obj);
        };

        checkoutData.getPickupAddressFromData = function () {
            var data = getData();
            return typeof data === 'object' ? data.pickupAddress : null;
        };

        checkoutData.setSelectedDropPointId = function (dropPointId) {
            var obj = getData();
            if (dropPointId === null || dropPointId === undefined) {
                obj.selectedDropPointId = dropPointId;
                saveData(obj);
                return checkoutData;
            }
            obj.selectedDropPointId = dropPointId;
            saveData(obj);
        };

        checkoutData.getSelectedDropPointId = function () {
            var data = getData();
            return typeof data === 'object' ? data.selectedDropPointId : null;
        };

        // Pickup form data
        checkoutData.pickup = null;

        checkoutData.setPickup = function(pickup) {
            var obj = getData();
            obj.pickup = pickup;
            saveData(obj);
        };

        checkoutData.getPickup = function() {
            var data = getData();

            if(typeof data === 'undefined' || (typeof data === 'object' && !data.hasOwnProperty('pickup'))) {
                return checkoutData.pickup;
            }

            return data.pickup;
        }

        return checkoutData;
    }
});
