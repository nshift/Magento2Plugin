/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'uiComponent',
    'Magento_Checkout/js/model/shipping-rates-validator',
    'Magento_Checkout/js/model/shipping-rates-validation-rules',
    '../../model/shipping-rates-validator/shipadvise',
    '../../model/shipping-rates-validation-rules/shipadvise'
], function (
    Component,
    defaultShippingRatesValidator,
    defaultShippingRatesValidationRules,
    shipadviseShippingRatesValidator,
    shipadviseShippingRatesValidationRules
) {
    'use strict';

    defaultShippingRatesValidator.registerValidator('shipadvise', shipadviseShippingRatesValidator);
    defaultShippingRatesValidationRules.registerRules('shipadvise', shipadviseShippingRatesValidationRules);

    return Component;
});
