var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/model/new-customer-address': {
                'Nshift_Integration/js/model/new-customer-address-mixin': true
            },
            'Magento_Checkout/js/view/cart/shipping-rates': {
                'Nshift_Integration/js/view/cart/shipping-rates-mixin': true
            },
            'Magento_Checkout/js/view/shipping': {
                'Nshift_Integration/js/view/shipping-mixin': true
            },
            'Magento_Checkout/js/checkout-data': {
                'Nshift_Integration/js/checkout-data-mixin': true
            },
            'Magento_Checkout/js/model/shipping-save-processor/payload-extender': {
                'Nshift_Integration/js/model/shipping-save-processor/payload-extender-mixin': true
            },
            'Magento_Checkout/js/view/shipping-information/address-renderer/default': {
                'Nshift_Integration/js/view/shipping-information/address-renderer/default-mixin': true
            },
            'Magento_Checkout/js/model/shipping-rates-validator': {
                'Nshift_Integration/js/model/shipping-rates-validator-mixin': true
            },
            'Magento_Checkout/js/model/checkout-data-resolver': {
                'Nshift_Integration/js/model/checkout-data-resolver-mixin': true
            }
        }
    }
};
