<?php
/**
 * CustomAttributeList
 *
 * @copyright Copyright © 2020 De Facto. All rights reserved.
 * @author    developers@ede-facto.com
 */

namespace Nshift\Integration\Model\Quote\Address;

class CustomAttributeList
{
    public const DROP_POINT_ID = 'droppoint_id';

    public const ATTRIBUTES = [
        self::DROP_POINT_ID,
        'dispatch_date',
        'delivery_date',
        'carrier',
        'carrier_display_name'
    ];

    protected $_attributes = null;


    public function getAttributes(): array
    {
        if ($this->_attributes === null) {
            foreach ($this->getAttributeCodes() as $attributeCode) {
                $this->_attributes[$attributeCode] = [
                    'attribute_code' => $attributeCode
                ];
            }
        }
        return $this->_attributes;
    }

    /**
     * @return string[]
     */
    public function getAttributeCodes(): array
    {
        return self::ATTRIBUTES;
    }
}
