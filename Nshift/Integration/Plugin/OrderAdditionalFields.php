<?php
namespace Nshift\Integration\Plugin;

use Magento\Sales\Model\Order\Address as BaseAddress;
/**
 * OrderAdditionalFields class
 */
class OrderAdditionalFields
{
    private $_checkoutSession;
    private $_salesOrder;
    /**
     * Place __construct
     *
     * @param string $checkoutSession //The checkoutSession
     * @param string $salesOrder      //The salesOrder
     *
     * @return null
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\Order $salesOrder
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_salesOrder = $salesOrder;
    }

    /**
     * AccessPoint afterBeforeSave
     *
     * @param \Magento\Sales\Model\Order\Address $subject //The subject
     * @param string                                        $result  //the proceed
     *
     * @return array $data
     */
    public function afterBeforeSave(BaseAddress $subject, BaseAddress $result): BaseAddress
    {
//        $additional_fields = array(
//            'droppoint_id',
//            'carrier',
//            'delivery_date',
//            'dispatch_date'
//        );
//
//        // Append fields
//        foreach($additional_fields as $field) {
//            $$field = $this->_checkoutSession->getData($field);
//
//            if (!empty($$field) && $result->getAddressType() === BaseAddress::TYPE_SHIPPING) {
//                $result->setData($field, $$field);
//            }
//        }
//
        return $result;
    }
}
