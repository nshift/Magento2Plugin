<?php
/**
 * ShippingInformationManagement
 *
 * @copyright Copyright © 2020 De Facto. All rights reserved.
 * @author    developers@ede-facto.com
 */

namespace Nshift\Integration\Plugin\Quote\Model;

use Nshift\Integration\Model\Quote\Address\CustomAttributeList;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\ResourceModel\Quote\Address;

class ShippingInformationManagement
{
    /**
     * @var CartRepositoryInterface
     */
    protected $quoteRepository;
    /**
     * @var Address
     */
    protected $addressResource;
    /**
     * @var CustomAttributeList
     */
    protected $customAttributeList;

    public function __construct(
        CustomAttributeList $customAttributeList,
        CartRepositoryInterface $quoteRepository,
        Address $addressResource
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->addressResource = $addressResource;
        $this->customAttributeList = $customAttributeList;
    }

    /**
     * Responsible for persisting the drop point id if selected.
     *
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param ShippingInformationInterface $addressInformation
     * @throws NoSuchEntityException
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    ) {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);

        if ($quote->isVirtual() || 0 == $quote->getItemsCount()) {
            return;
        }
        $shippingAddress = $quote->getShippingAddress();
        $dropPointAttr = $addressInformation->getShippingAddress()
            ->getCustomAttribute(CustomAttributeList::DROP_POINT_ID);
        if ($dropPointAttr) {
            $shippingAddress->setData(CustomAttributeList::DROP_POINT_ID, $dropPointAttr->getValue());
        } else {
            $shippingAddress->setData(CustomAttributeList::DROP_POINT_ID, null);
        }
        $this->addressResource->save($shippingAddress);
    }
}
