<?php
/**
 * ToOrderAddress
 *
 * @copyright Copyright © 2020 De Facto. All rights reserved.
 * @author    developers@ede-facto.com
 */

namespace Nshift\Integration\Plugin\Quote\Model\Address;

use Nshift\Integration\Model\Quote\Address\CustomAttributeList;
use Magento\Framework\DataObject\Copy;
use Magento\Quote\Model\Quote\Address;
use Magento\Sales\Api\Data\OrderAddressInterface;

class ToOrderAddress
{

    /**
     * @var Copy
     */
    protected $objectCopyService;
    /**
     * @var CustomAttributeList
     */
    protected $customAttributeList;

    public function __construct(
        Copy $objectCopyService,
        CustomAttributeList $customAttributeList
    ) {
        $this->objectCopyService = $objectCopyService;
        $this->customAttributeList = $customAttributeList;
    }

    public function afterConvert(
        Address\ToOrderAddress $subject,
        OrderAddressInterface $orderAddress,
        Address $object,
        $data = []
    ): OrderAddressInterface {

        $orderAddressData = $this->objectCopyService->getDataFromFieldset(
            'sales_convert_quote_address',
            'to_order_address',
            $object
        );

        foreach ($this->customAttributeList->getAttributeCodes() as $attributeCode) {
            if (isset($orderAddressData[$attributeCode])) {
                $orderAddress->setData($attributeCode, $orderAddressData[$attributeCode]);
            }
        }

        return $orderAddress;
    }
}
