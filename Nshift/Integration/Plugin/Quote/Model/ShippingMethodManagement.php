<?php
/**
 * ShippingMethodManagement
 *
 * @copyright Copyright © 2020 De Facto. All rights reserved.
 * @author    developers@ede-facto.com
 */

namespace Nshift\Integration\Plugin\Quote\Model;

use Nshift\Integration\Model\Quote\Address\CustomAttributeList;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\EstimateAddressInterface;
use Magento\Quote\Model\ResourceModel\Quote\Address;

class ShippingMethodManagement
{
    /**
     * @var CartRepositoryInterface
     */
    protected $quoteRepository;
    /**
     * @var Address
     */
    protected $addressResource;
    /**
     * @var CustomAttributeList
     */
    protected $customAttributeList;

    public function __construct(
        CustomAttributeList $customAttributeList,
        CartRepositoryInterface $quoteRepository,
        Address $addressResource
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->addressResource = $addressResource;
        $this->customAttributeList = $customAttributeList;
    }

    /**
     * @param \Magento\Quote\Model\ShippingMethodManagement $subject
     * @param $cartId
     * @param AddressInterface $address
     * @return array|mixed
     * @throws AlreadyExistsException
     * @throws NoSuchEntityException
     */
    public function beforeEstimateByExtendedAddress(
        \Magento\Quote\Model\ShippingMethodManagement $subject,
        $cartId,
        AddressInterface $address
    ) {
        return $this->_updateDropPoint($cartId, $address);
    }

    /**
     * @param \Magento\Quote\Model\ShippingMethodManagement $subject
     * @param $cartId
     * @param EstimateAddressInterface $address
     * @return array|mixed
     * @throws AlreadyExistsException
     * @throws NoSuchEntityException
     */
    public function beforeEstimateByAddress(
        \Magento\Quote\Model\ShippingMethodManagement $subject,
        $cartId,
        EstimateAddressInterface $address
    ) {
        return $this->_updateDropPoint($cartId, $address);
    }

    /**
     * @param \Magento\Quote\Model\ShippingMethodManagement $subject
     * @param $cartId
     * @param $addressId
     * @return array|mixed
     * @throws AlreadyExistsException
     * @throws NoSuchEntityException
     */
    public function beforeEstimateByAddressId(
        \Magento\Quote\Model\ShippingMethodManagement $subject,
        $cartId,
        $addressId
    ) {
        return $this->_updateDropPoint($cartId, null, true);
    }

    /**
     * @param $cartId
     * @param null|EstimateAddressInterface|AddressInterface $address
     * @param bool $removeDropPoint
     * @return mixed
     * @throws AlreadyExistsException
     * @throws NoSuchEntityException
     */
    protected function _updateDropPoint($cartId, $address = null, $removeDropPoint = false)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);

        // no methods applicable for empty carts or carts with virtual products
        if ($quote->isVirtual() || 0 == $quote->getItemsCount()) {
            return [];
        }

        $shippingAddress = $quote->getShippingAddress();
        if (!$removeDropPoint && $dropPointAttr = $address->getCustomAttribute(CustomAttributeList::DROP_POINT_ID)) {
            $shippingAddress->setData(CustomAttributeList::DROP_POINT_ID, $dropPointAttr->getValue());
        } elseif ($shippingAddress->getData(CustomAttributeList::DROP_POINT_ID)) {
            // The reason for this condition is to clear out any persisted data from the drop point that
            // was previously selected.
            // This can happen if you've gone to the payment step of checkout and selected a drop point
            // but then returned to the postal address form
            foreach ($this->customAttributeList->getAttributeCodes() as $attributeCode) {
                $shippingAddress->setData($attributeCode, null);
            }
            $shippingAddress->setShippingMethod(null)
                ->setShippingDescription(null)
                ->setCompany(null)
                ->setRegionCode(null)
                ->setRegionId(null);
        }
        $this->addressResource->save($shippingAddress);
    }
}
