<?php
/**
 * PluginAccessPoint file
 *
 * @category  UPS_Shipping
 * @package   UPS_Shipping
 * @author  Nshift <integrations@nshift.com>
 * @copyright 2021 Nshift, all rights reserved
 */
namespace Nshift\Integration\Plugin\Quote\Address;

/**
 * Class Rate
 *
 */
class Rate
{
    /**
     * @var \Nshift\Integration\Model\Quote\Address\CustomAttributeList
     */
    protected $customAttributeList;

    public function __construct(
        \Nshift\Integration\Model\Quote\Address\CustomAttributeList $customAttributeList
    ) {
        $this->customAttributeList = $customAttributeList;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\AbstractResult $rate
     * @return \Magento\Quote\Model\Quote\Address\Rate
     */
    public function afterImportShippingRate($subject, $result, $rate)
    {
        if ($rate instanceof \Magento\Quote\Model\Quote\Address\RateResult\Method) {
            $result->setDescription(
                $rate->getDescription()
            );

            $result->setAdditional(
                $rate->getAdditional()
            );
            foreach ($this->customAttributeList->getAttributeCodes() as $attributeCode) {
                if ($attributeCode === 'carrier') { // carrier is a reserved attribute for a shipping rate
                    continue;
                }
                if ($value = $rate->getData($attributeCode)) {
                    $result->setData($attributeCode, $value);
                }
            }
        }

        return $result;
    }
}
