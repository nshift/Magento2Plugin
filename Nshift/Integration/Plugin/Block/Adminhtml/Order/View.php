<?php
namespace Nshift\Integration\Plugin\Block\Adminhtml\Order;

class View
{


    public function beforePushButtons(
        \Magento\Backend\Block\Widget\Button\Toolbar\Interceptor $subject,
        \Magento\Framework\View\Element\AbstractBlock $context,
        \Magento\Backend\Block\Widget\Button\ButtonList $buttonList
    ) {

        if($context->getRequest()->getFullActionName() == 'sales_order_view') {
            $params = $context->getRequest()->getParams();

            if(!empty($params['order_id'])) {
                $ship_order_url = $context->getUrl('nshiftintegration/shiporders/index', ['order_ids' => $params['order_id']]);

                $buttonList->add(
                    'ship_with_nshift',
                    [
                        'label' => __('Ship with Nshift'),
                        'onclick' => 'setLocation("' . $ship_order_url . '")',
                        'class' => 'reset'
                    ],
                    -1
                );
            }
        }
    }
}
