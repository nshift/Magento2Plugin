<?php
namespace Nshift\Integration\Plugin\Block\Checkout;

class DirectoryDataProcessorPlugin
{
    protected $shipadvise;
    protected $_customer;
    protected $_customerRepository;
    protected $_addressRepository;
    /**
     * @var \Magento\Directory\Helper\Data
     */
    protected $directoryHelper;

    public function __construct(
        \Magento\Directory\Helper\Data $directoryHelper,
        \Nshift\Integration\Model\Carrier\Shipadvise $carrierModel,
        \Magento\Customer\Model\Session $customer,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository
    ) {
        $this->_shipadvise = $carrierModel;
        $this->_customer = $customer;
        $this->_customerRepository = $customerRepository;
        $this->_addressRepository = $addressRepository;
        $this->directoryHelper = $directoryHelper;
    }

    /**
     * @param \Magento\Checkout\Block\Checkout\DirectoryDataProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\DirectoryDataProcessor $subject,
        array  $jsLayout
    ) {
        if($this->_shipadvise->isActive()) {
            if($this->_shipadvise->getUseDeliverTo()) {
                // Add country options to pickup form
                $countryId = &$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']['children']['pickup-address-form-fields']['children']['country_id'];
                $countryId['defaultValue'] = $this->directoryHelper->getDefaultCountry();
                $countryId['value'] = $this->directoryHelper->getDefaultCountry();

                $countryId['deps'] = ['checkoutProvider'];
                $countryId['imports'] = [
                    'initialOptions' => 'index = checkoutProvider:dictionaries.country_id',
                    'setOptions' => 'index = checkoutProvider:dictionaries.country_id'
                ];

                // Add region options to pickup form
                $region = &$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']['children']['pickup-address-form-fields']['children']['region_id'];

                $region['deps'] = ['checkoutProvider'];
                $region['imports'] = [
                    'initialOptions' => 'index = checkoutProvider:dictionaries.region_id',
                    'setOptions' => 'index = checkoutProvider:dictionaries.region_id'
                ];

                // Prefill logged in customer data
                if ($this->_customer->isLoggedIn()) {
                    $customer_data = $this->_customer->getCustomerData();

                    $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']['children']['personal-details-form-fields']['children']['firstname']['value'] = $customer_data->getFirstname();

                    $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']['children']['personal-details-form-fields']['children']['lastname']['value'] = $customer_data->getLastname();

                    $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']['children']['personal-details-form-fields']['children']['email']['value'] = $customer_data->getEmail();

                    // Prefill default shipping address data
                    $shipping_address_id = $customer_data->getDefaultShipping();

                    if (!empty($shipping_address_id)) {
                        try {
                            $shipping_address = $this->_addressRepository->getById($shipping_address_id);

                            if (!empty($shipping_address->getCountryId())) {
                                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']['children']['pickup-address-form-fields']['children']['country_id']['value'] = $shipping_address->getCountryId();
                            }

                            if (!empty($shipping_address->getRegionId())) {
                                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']['children']['pickup-address-form-fields']['children']['region_id']['value'] = $shipping_address->getRegionId();
                            }

                            if (!empty($shipping_address->getCity())) {
                                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']['children']['pickup-address-form-fields']['children']['city']['value'] = $shipping_address->getCity();
                            }

                            if (!empty($shipping_address->getPostcode())) {
                                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']['children']['pickup-address-form-fields']['children']['postcode']['value'] = $shipping_address->getPostcode();
                            }

                            if (!empty($shipping_address->getTelephone())) {
                                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']['children']['personal-details-form-fields']['children']['telephone']['value'] = $shipping_address->getTelephone();
                            }
                        } catch (\Exception $e) {
                            //
                        }
                    }

                    // Hide email for logged in customers
                    $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']['children']['pickup-address-form-fields']['children']['email']['config']['elementTmpl'] = 'ui/form/element/hidden';
                    $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']['children']['pickup-address-form-fields']['children']['email']['label'] = '';
                    $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']['children']['pickup-address-form-fields']['children']['email']['validation']['required-entry'] = false;
                    $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']['children']['pickup-address-form-fields']['children']['email']['sortOrder'] = 99;
                }

                // Set shipping method template
                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['config']['shippingMethodItemTemplate'] = 'Nshift_Integration/shipping-method/deliverto-method-item';
                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['config']['shippingMethodListTemplate'] = 'Nshift_Integration/shipping-method/deliverto-method-list';
            } else {
                // Remove deliver_to and pickup point form
                unset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['deliver_to']);
                unset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']);
            }
        } else {
            // Remove deliver_to and pickup point form
            unset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['deliver_to']);
            unset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['before-form']['children']['pickup-point-form-container']);
        }

        return $jsLayout;
    }
}
