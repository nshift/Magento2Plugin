<?php

namespace Nshift\Integration\Controller\Adminhtml\Order;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Sales\Api\OrderManagementInterface;

class ShipWithNshift extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{

    protected $orderManagement;

    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        OrderManagementInterface $orderManagement
    ) {
        parent::__construct($context, $filter);
        $this->collectionFactory = $collectionFactory;
        $this->orderManagement = $orderManagement;
    }

    protected function massAction(AbstractCollection $collection)
    {
        $selected_orders = 0;

        $order_ids = array();

        foreach ($collection->getItems() as $order) {
            if (!$order->getEntityId()) {
                continue;
            }

            $order_ids[] = $order->getEntityId();

            $selected_orders++;
        }

        if ($selected_orders) {
            $this->messageManager->addSuccess(__('You are currently shipping %1 order(s).', $selected_orders));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('nshiftintegration/shiporders/index', ['order_ids' => implode(',',$order_ids)]);

        return $resultRedirect;
    }
}