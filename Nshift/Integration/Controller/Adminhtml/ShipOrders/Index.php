<?php
namespace Nshift\Integration\Controller\Adminhtml\ShipOrders;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Load the page defined in view/adminhtml/layout/nshiftintegration_ship_orders_index.xml
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $page = $resultPage = $this->resultPageFactory->create();

        $block = $page->getLayout()->getBlock('config_view');

        $parameters = $this->getRequest()->getParams();

        if(!empty($parameters['order_ids'])) {
            $block->setData('order_ids', $parameters['order_ids']);
        }

        return $page;
    }
}