<?php

namespace Nshift\Integration\Controller\Eshopper;

class SaveAdditionalFields extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $checkoutSession;
    protected $countryFactory;
    protected $carrier;
    protected $scopeConfig;
    protected $_helperData;

    /**
     * GetAllDropPoints __construct
     *
     * @param string $context           //The context
     * @param string $resultJsonFactory //The resultJsonFactory
     * @param string $countryFactory    //The countryFactory
     * @param string $checkoutSession   //The checkoutSession
     * @param string $carrier           //The carrier
     * @param string $scopeConfig       //The scopeConfig
     *
     * @return null
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Nshift\Integration\Model\Carrier\Shipadvise $carrier,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Nshift\Integration\Helper\Data $helperData
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->checkoutSession = $checkoutSession;
        $this->countryFactory = $countryFactory;
        $this->carrier = $carrier;
        $this->scopeConfig = $scopeConfig;
        $this->_helperData = $helperData;
        parent::__construct($context);
    }

    /**
     * saveAdditionalFields execute
     *
     * @return boolean
     */
    public function execute()
    {
        if ($this->getRequest()->isAjax()) {
            $result = $this->resultJsonFactory->create();
            $params = $this->getRequest()->getParams();

            // Parse dates for database
            if(!empty($params['delivery_date'])) {
                $params['delivery_date'] = date('Y-m-d H:i:s', strtotime($params['delivery_date']));
            }

            if(!empty($params['cutoff_time'])) {
                $params['cutoff_time'] = date('Y-m-d H:i:s', strtotime($params['cutoff_time']));
            }

            if(!empty($params['carrier_display_name'])) {
                $params['carrier_display_name'] = $params['carrier_display_name'];
            }

            $this->checkoutSession->setData('carrier', $params['carrier']);
            $this->checkoutSession->setData('delivery_date', $params['delivery_date']);
            $this->checkoutSession->setData('dispatch_date', $params['dispatch_date']);
            $this->checkoutSession->setData('carrier_display_name', $params['carrier_display_name']);

            $response = array(
                'success' => 'Session saved succesfully! - ' . json_encode($params)
            );

            return $result->setData($response);
        }
    }
}
