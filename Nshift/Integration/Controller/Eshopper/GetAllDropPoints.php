<?php
/**
 * GetAllDropPoints file
 *
 * @category  Nshift_Integration
 * @package   Nshift_Integration
 * @author  Nshift <integrations@nshift.com>
 * @copyright 2021 Nshift, all rights reserved
 */
namespace Nshift\Integration\Controller\Eshopper;
/**
 * GetAllDropPoints class
 *
 * @category  Nshift_Integration
 * @package   Nshift_Integration
 * @author  Nshift <integrations@nshift.com>
 * @copyright 2021 Nshift, all rights reserved
 */
class GetAllDropPoints extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $checkoutSession;
    protected $countryFactory;
    protected $carrier;
    protected $scopeConfig;
    protected $_helperData;
    protected $_regionFactory;
    protected $_storeManager;

    /**
     * GetAllDropPoints __construct
     *
     * @param string $context           //The context
     * @param string $resultJsonFactory //The resultJsonFactory
     * @param string $countryFactory    //The countryFactory
     * @param string $checkoutSession   //The checkoutSession
     * @param string $carrier           //The carrier
     * @param string $scopeConfig       //The scopeConfig
     *
     * @return null
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Nshift\Integration\Model\Carrier\Shipadvise $carrier,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Nshift\Integration\Helper\Data $helperData,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->checkoutSession = $checkoutSession;
        $this->countryFactory = $countryFactory;
        $this->carrier = $carrier;
        $this->scopeConfig = $scopeConfig;
        $this->_helperData = $helperData;
        $this->_cart = $cart;
        $this->_regionFactory = $regionFactory;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * GetAllDropPoints execute
     *
     * @return boolean
     */
    public function execute()
    {
        if ($this->getRequest()->isAjax()) {
            $result = $this->resultJsonFactory->create();
            $params = $this->getRequest()->getParams();

            // Build request data
            if (!empty($params['region_id'])) {
                $region_code = $this->getRegionCode($params['region_id']);
            } else {
                $region_code = '';
            }

            // Attribute qa or prod token
            $token = $this->_helperData->getToken();
            $webservice_endpoint = 'https://integration.consignor.com';

            if(!empty($this->_helperData->getDebug())) {
                $token = $this->_helperData->getQAToken();
                $webservice_endpoint = 'http://int.qa.consignor.com';
            }

            $json = array(
                'token' => $token,
                'version' => $this->_helperData->getVersion(),
                'magento_version' => $this->_helperData->getMagentoVersion(),
                'currency' => $this->_storeManager->getStore()->getCurrentCurrency()->getCode(),
                'ups_delivery_date_required' => $this->_helperData->getUPSDeliveryDateRequired(),
                'receiver' => array(
                    'attention' => '',
                    'name' => $params['firstname'] . ' ' . $params['lastname'],
                    'company' => '',
                    'street1' => '',
                    'street2' => '',
                    'city' => $params['city'],
                    'region' => $region_code,
                    'postcode' => $params['postcode'],
                    'country_code' => $params['country_id'],
                    'phone' => $params['telephone'],
                    'mobile' => $params['telephone'],
                    'email' => $params['email']
                ),
                'products' => []
            );

            $products = [];

            foreach ($this->_cart->getQuote()->getAllItems() as $_item) {
                if (!$_item->getParentItemId() && !$_item->getParentItem()) {
                    $products[] = [
                        'quantity' => $_item->getQty(),
                        'weight' => $_item->getWeight(),
                        'ship_separately' => $_item->isShipSeparately(),
                        'has_children' => $_item->getHasChildren(),
                        'is_virtual' => $_item->getProduct()->isVirtual(),
                    ];
                }
            }

            $json['products'] = $products;
            $json = json_encode($json);

            // Curl request
            $ch = curl_init($webservice_endpoint . '/magento/get-all-drop-points');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($json))
            );

            $response = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($response, 1);
            $response['request'] = $json;

            return $result->setData($response);
        }
    }

    public function getRegionCode($region_id){
        $region_code = null;

        if(!empty($region_id)) {
            $region = $this->_regionFactory->create()->load($region_id);
            $region_code = $region->getCode();
        }

        return $region_code;
    }
}
