<?php
/**
 * Review
 *
 * @copyright Copyright © 2020 De Facto. All rights reserved.
 * @author    developers@ede-facto.com
 */

namespace Nshift\Integration\ViewModel\PayPal\Express;

use Nshift\Integration\Model\Quote\Address\CustomAttributeList;
use Magento\Framework\DataObject;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class Review implements ArgumentInterface
{
    /**
     * @var SerializerInterface
     */
    protected $serializer;
    /**
     * @var CustomAttributeList
     */
    protected $customAttributeList;

    public function __construct(SerializerInterface $serializer, CustomAttributeList $customAttributeList)
    {
        $this->serializer = $serializer;
        $this->customAttributeList = $customAttributeList;
    }

    /**
     * @param array|null $groupedRates
     * @param false $toJson
     * @return array|string
     */
    public function getFormattedRates(?array $groupedRates, $toJson = false)
    {
        $_rates = [];
        if ($groupedRates) {
            foreach ($groupedRates as $code => $rates) {
                foreach ($rates as $rate) {
                    if ($rate instanceof DataObject) {
                        $_rates[] = $rate->getData();
                    }
                }
            }
        }

        return $toJson ? $this->serializer->serialize($_rates) : $_rates;
    }

    public function getFormattedRate($rate, $toJson = false)
    {
        if ($rate instanceof DataObject) {
            return $toJson ? $this->serializer->serialize($rate->getData()) : $rate->getData();
        }
        return null;
    }

    public function getAttributeCodes(): array
    {
        return $this->customAttributeList->getAttributeCodes();
    }
}
