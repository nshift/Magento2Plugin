<?php
/**
 * A Magento 2 module named Nshift/Integration
 * Copyright (C) 2017  Nshift 2018
 *
 * This file is part of Nshift/Integration.
 *
 * Nshift/Integration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Nshift\Integration\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\App\ProductMetadataInterface;

class Data extends AbstractHelper
{
    const XML_PATH_HELLOWORLD = 'Nshift/Integration';
    const MODULE_NAME = 'Nshift_Integration';
    public const XML_PATH_CARRIERS_SHIPADVISE_ACTIVE = 'carriers/shipadvise/active';
    public const XML_PATH_CARRIERS_SHIPADVISE_DEBUG = 'carriers/shipadvise/debug';
    public const XML_PATH_CARRIERS_SHIPADVISE_TOKEN = 'carriers/shipadvise/token';
    public const XML_PATH_CARRIERS_SHIPADVISE_QATOKEN = 'carriers/shipadvise/qatoken';
    public const XML_PATH_CARRIERS_SHIPADVISE_UPSDELIVERYDATEREQUIRED = 'carriers/shipadvise/upsdeliverydaterequired';
    public const XML_PATH_CARRIERS_SHIPADVISE_USEDELIVERTO = 'carriers/shipadvise/usedeliverto';
    public const XML_PATH_CARRIERS_SHIPADVISE_GOOGLEMAPSAPIKEY = 'carriers/shipadvise/googlemapsapikey';

    protected $_moduleList;

    public function __construct(
        Context $context,
        ModuleListInterface $moduleList,
        ProductMetadataInterface $productMetadata
    )
    {
        $this->productMetadata = $productMetadata;
        $this->_moduleList = $moduleList;
        parent::__construct($context);
    }

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    public function getGeneralConfig($code, $storeId = null)
    {

        return $this->getConfigValue(self::XML_PATH_HELLOWORLD . 'general/'. $code, $storeId);
    }

    public function getVersion()
    {
        if(!empty($this->_moduleList->getOne(self::MODULE_NAME)['setup_version'])) {
            return $this->_moduleList->getOne(self::MODULE_NAME)['setup_version'];
        }

        return null;
    }

    public function getMagentoVersion()
    {
        if(!empty($this->productMetadata->getVersion())) {
            return $this->productMetadata->getVersion();
        }

        return null;
    }

    public function isShipAdviseEnabled(): bool
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_CARRIERS_SHIPADVISE_ACTIVE,
            'website'
        );
    }

    public function getDebug():? string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_CARRIERS_SHIPADVISE_DEBUG,
            'website'
        );
    }

    public function getToken():? string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_CARRIERS_SHIPADVISE_TOKEN,
            'website'
        );
    }

    public function getQAToken():? string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_CARRIERS_SHIPADVISE_QATOKEN,
            'website'
        );
    }

    public function getUPSDeliveryDateRequired():? string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_CARRIERS_SHIPADVISE_UPSDELIVERYDATEREQUIRED,
            'website'
        );
    }

    public function getUseDeliverTo():? string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_CARRIERS_SHIPADVISE_USEDELIVERTO,
            'website'
        );
    }

    public function getGoogleMapsApiKey():? string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_CARRIERS_SHIPADVISE_GOOGLEMAPSAPIKEY,
            'website'
        );
    }

    public function getGoogleMapOptions(): array
    {
        return [
            'mapTypeId' => 'roadmap'
        ];
    }
}
